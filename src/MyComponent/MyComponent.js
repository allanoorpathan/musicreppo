import React from 'react';
import { Text, View } from 'react-native';
import { useTrackPlayerProgress } from 'react-native-track-player';

export  const MyComponent = () => {
  const { position, bufferedPosition, duration } = useTrackPlayerProgress()

  return (
    <View>
      <Text>Track progress: {position} seconds out of {duration} total</Text>
      <Text>Buffered progress: {bufferedPosition} seconds buffered out of {duration} total</Text>
    </View>
  )
}
