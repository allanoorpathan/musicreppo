import React from 'react'

import {
    /* previously imported modules here */
    Animated, // add this
    Easing // add this
    ,Dimensions
  } from "react-native";
  const { height, width } = Dimensions.get("window");

  type Props = {};
  export default class AnimatedModal extends React.Component<Props> {
    constructor(props) {
        super(props);
        this.yTranslate = new Animated.Value(0); // declare animated value for controlling the vertical position of the modal
      }
  
      render() {
        const { title, image, children, onClose } = this.props;
  
        let negativeHeight = -height + 20;
        let modalMoveY = this.yTranslate.interpolate({
          inputRange: [0, 1],
          outputRange: [0, negativeHeight]
        });
  
        let translateStyle = { transform: [{ translateY: modalMoveY }] }; // translateY is the transform for moving objects vertically
        // next: render the component
      }
  }
  const styles = {
    container: {
      position: "absolute",
      height: height,
      width: width,
      bottom: -height, // look here
      backgroundColor: "#fff"
    }
    // ... other styles
  };