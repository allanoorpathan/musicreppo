import React from 'react'
import { View, Text, Button, Image, StyleSheet, TouchableOpacity } from 'react-native'
import SoundPlayer from 'react-native-sound-player'
import TrackPlayer from 'react-native-track-player';
import { Icon } from 'react-native-elements'

var track= {
  id: 'unique track id', // Must be a string, required
 // url: 'https://dl.espressif.com/dl/audio/ff-16b-2c-11025hz.mp3', // Load media from the network
  url: require('../images/delivery.jpg'), // Load media from the app bundle
  url: 'https://dl.espressif.com/dl/audio/ff-16b-2c-11025hz.mp3', // Load media from the file system 
  title: 'my name is khan',
  artist: 'deadmau5',
  album: 'while(1<2)',
  genre: 'Progressive House, Electro House',
  date: '2014-05-20T07:00:00+00:00', // RFC 3339
  artwork: 'http://example.com/avaritia.png', // Load artwork from the network
  artwork: require('../images/delivery.jpg'), // Load artwork from the app bundle
 // artwork: 'https://dl.espressif.com/dl/audio/ff-16b-2c-11025hz.mp3' // Load artwork from the file system
};
var track1= {
  id: 'unique track id', // Must be a string, required
 // url: 'https://dl.espressif.com/dl/audio/ff-16b-2c-11025hz.mp3', // Load media from the network
  url: require('../images/delivery.jpg'), // Load media from the app bundle
  url: 'https://ia600204.us.archive.org/11/items/hamlet_0911_librivox/hamlet_act2_shakespeare.mp3', // Load media from the file system 
  title: 'my name is khan',
  artist: 'deadmau5',
  album: 'while(1<2)',
  genre: 'Progressive House, Electro House',
  date: '2014-05-20T07:00:00+00:00', // RFC 3339
  artwork: 'http://example.com/avaritia.png', // Load artwork from the network
  artwork: require('../images/delivery.jpg'), // Load artwork from the app bundle
 // artwork: 'https://dl.espressif.com/dl/audio/ff-16b-2c-11025hz.mp3' // Load artwork from the file system
};

export default class home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      // trackTitle:''
      isPlaying: false,
      playbackInstance: null,
      currentIndex: 0,
      volume: 1.0,
      isBuffering: false
    }
    TrackPlayer.setupPlayer().then(() => {
    });
    TrackPlayer.add([track,track1]).then(function () {

    });

  }

  componentDidMount() {
    // Adds an event handler for the playback-track-changed event
    TrackPlayer.setVolume(1.9);
    this.onTrackChange = TrackPlayer.addEventListener('playback-track-changed', async (data) => {
      const track = await TrackPlayer.getTrack(data.nextTrack);
      let position = await TrackPlayer.getPosition();
      let buffered = await TrackPlayer.getBufferedPosition();
      let duration = await TrackPlayer.getDuration();

      console.log('TAG: first-------:' + buffered+"-----------"+duration);

      var updateState = { ...this.state }
      updateState.trackTitle = track.title
      updateState.position = position
      updateState.buffered = buffered
      updateState.duration = duration
      this.setState(updateState);


    });
    this.stateChange();
  }
  componentWillUnmount() {
    // Removes the event handler
    this.onTrackChange.remove();
  }

  stateChange = async () => {
    let state = await TrackPlayer.getState();

    let trackId = await TrackPlayer.getCurrentTrack();
    let trackObject = await TrackPlayer.getTrack(trackId);
    console.log('TAG: second:----------' + state, trackId, trackObject)
    // Position, buffered position and duration return values in seconds

  }
  PlayeSong = () => {
    TrackPlayer.play();
    this.setState({
      isPlaying: true
    })
  }
  PouseSong = () => {
    TrackPlayer.stop();

    this.setState({
      isPlaying: false
    })
  }

  nextSong = () => {
    TrackPlayer.skipToNext();
  }
  previousSong=()=>{
    TrackPlayer.skipToPrevious()

  }

  renderFileInfo() {
    const { currentIndex } = this.state
    return  (
      <View style={styles.trackInfo}>
       
       
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.albumCover}
          source={{ uri: 'http://www.archive.org/download/LibrivoxCdCoverArt8/hamlet_1104.jpg' }}
        />
        <View style={styles.controls}>
            <TouchableOpacity style={styles.control} onPress={this.nextSong}>
              <Icon name='backward' type='font-awesome' size={30} color='#444' />
            </TouchableOpacity>
            <TouchableOpacity style={styles.control} >
              {this.state.isPlaying ? (
                <Icon name='pause' type='font-awesome' onPress={this.PouseSong} size={30} color='#444' />
              ) : (
                  <Icon name='play' type='font-awesome' onPress={this.PlayeSong} size={30} color='#444' />
                )}
            </TouchableOpacity>
            <TouchableOpacity style={styles.control} onPress={this.previousSong}>
              <Icon name='forward' type='font-awesome' size={30} color='#444' />
            </TouchableOpacity>

        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',


  },
  albumCover: {
    width: 300,
    height: 250
  },
  controls: {
    flexDirection: 'row',
    backgroundColor:'#dae7da',
    width: 300,
    alignItems: 'center',
    justifyContent: 'center'
  },
  control: {
    margin: 20
  },
  trackInfo: {
    padding: 40,
    backgroundColor: '#fff'
  },
  trackInfoText: {
    textAlign: 'center',
    flexWrap: 'wrap',
    color: '#550088'
  },
  largeText: {
    fontSize: 22
  },
  smallText: {
    fontSize: 16
  },
  control: {
    margin: 20
  },
  
})