import React from 'react';
import { Text, View, StyleSheet, Image, Dimensions, TouchableWithoutFeedback, Animated, Easing  } from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import SlidingUpPanel from 'rn-sliding-up-panel';
import AnimatedModal from '../Animation/AnimatedModal';
import Handle from '../Handle'
import { Icon } from 'react-native-elements'


const { height } = Dimensions.get('window')
export default class AllSongList extends React.Component {
  constructor() {
    super();
    this.state = {
      songList: [
        { image: require('../images/delivery.jpg'), songTitle: "tere bina dil naa lage" },
        { image: require('../images/delivery.jpg'), songTitle: "mere pass tooo aa raja" },
        { image: require('../images/delivery.jpg'), songTitle: "tu hee kaminauife uqwjkdb" },
        
      ],
    };
  }
  CallAnimation=()=>{
    let scaleValue = new Animated.Value(0);
    const cardScale = scaleValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 1.1, 1.2]
    });
    let transformStyle = { ...styles.card, transform: [{ scale: cardScale }] };
    return(
      <TouchableWithoutFeedback>
        <Animated.View style={transformStyle}>
          {/* <Image source={require('../images/delivery.jpg')} style={styles.thumbnail} /> */}
          <Text style={styles.name}>{'jakir'}</Text>
          <View style={styles.icons}>
            <Icon
              icon="search"
              
              data={'name'}
            />
            <Icon icon="bookmark"  data={'saddam'} />
            <Icon icon="share"  data={'faruk'} />
          </View>
        </Animated.View>
      </TouchableWithoutFeedback>
    )
  }
  renderItemSong = ({ item, index }) => {
    return (
      <View style={{ flexDirection: 'row', backgroundColor: '#374874', paddingHorizontal: 10, borderBottomColor: 'grey', borderBottomWidth: 0.6, paddingVertical: 8, marginHorizontal: 20, }}>
        <View style={{ flex: 0.3 }}>
          <Image style={{ height: 40, width: 40 }} source={item.image} />
        </View>
        <View style={{ flex: 1.4 }}>
          <Text style={{ color: 'grey', fontSize: 16 }}>{item.songTitle}</Text>
        </View>
        <View style={{ flex: 0.2 }}>
          <Image style={{ width: 20, height: 20 }} source={require('../images/delivery.jpg')} />
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ backgroundColor: '#000', flexDirection: 'row', paddingVertical: 25, justifyContent: 'space-between', paddingHorizontal: 10, }}>
          <View>
            <Image style={{ width: 23, height: 23 }} source={require('../images/delivery.jpg')} />
          </View>
          <View>
            <Image style={{ width: 23, height: 23, margin: -10 }} source={require('../images/delivery.jpg')} />
          </View>
          <View>
            <Image style={{ width: 23, height: 23 }} source={require('../images/delivery.jpg')} />
          </View>
        </View>

        <View style={{ backgroundColor: '#000', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20, paddingBottom: 13 }}>
          <View>
            <Text style={{ color: "#fff", fontSize: 23, fontWeight: 'bold' }}>Road</Text>
          </View>
          <View>
            <Image style={{ width: 23, height: 23 }} source={require('../images/delivery.jpg')} />
          </View>
        </View>

        <View style={{ flexDirection: 'row', marginHorizontal: 10, backgroundColor: '#e6438c', paddingVertical: 10, paddingBottom: 50 }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>other</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Road</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Save</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Classic</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Music-Trap</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Song-Trap</Text>
            <Text style={{ color: '#f78cd4', fontSize: 16, marginLeft: 10 }}>Dowload-Trap</Text>
          </ScrollView>
        </View>
        
        <View style={{ marginHorizontal: 10, margin: -30, }}>
          <FlatList
            data={this.state.songList}
            renderItem={this.renderItemSong}
            showsVerticalScrollIndicator={false}
            extraData={this.state}
            legacyImplementation={true}
            keyExtractor={(index) => { index.toString(); }}
          />
        </View>
        <AnimatedModal/>
        {/* <SlidingUpPanel
          ref={c => (this._panel = c)}
          draggableRange={{ top: height / 1.1, bottom:80 }}
          animatedValue={this._draggedValue}
          showBackdrop={false}>
          <View style={styles.panel}>
            <View style={styles.panelHeader}>
              <View style={{ flexDirection: "row",marginHorizontal:15 ,alignItems:'center',marginVertical:10}}>
                <View style={{flex:0.2}}>
                  <Image style={{ width: 20, height: 20 }} source={require('../images/delivery.jpg')} />
                </View>
                <View style={{flex:1,marginRight:10}}>
                  <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: 'bold', color: '#fff' }}>Daft punk-Get Lunky iuqey981y23e713ye</Text>
                </View>
                <ImageBackground imageStyle={{ borderRadius: 100}} style={{ height: 50, width: 50 ,alignItems:'center',justifyContent:'center', borderRadius: 100}} source={require('../images/delivery.jpg')}>
                  <Image style={{ width: 20, height: 20 }} source={require('../images/delivery.jpg')} />
                </ImageBackground>
              </View>
            </View>
          </View>
        </SlidingUpPanel> */}
        {this.CallAnimation()}
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, backgroundColor: '#000'
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative'
  },
  panelHeader: {
    height: "100%",
    backgroundColor: '#697e9e',
  },
});