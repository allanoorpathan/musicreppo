import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import home from '../home/home';
import AllSongList from '../AllSongLIst/AllSongList'

export default class navigation extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <AllSongList></AllSongList>
            </NavigationContainer>
        );
    }
}