/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import home from './src/home/home';
import navigation  from './src/navigation/index'
//import AllSongList from './src/AllSongList/AllSongList'

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => navigation);
